RSpec.describe "Potepan::Products", type: :request do
  describe "GET #show" do
    let(:product) { create(:product) }

    before do
      get potepan_product_path(product.id)
    end

    it "returns a 200 response" do
      expect(response).to have_http_status(200)
    end

    it "show the product name" do
      expect(response.body).to include product.name
    end

    it "show the product price" do
      expect(response.body).to include product.display_price.to_s
    end

    it "show the product description" do
      expect(response.body).to include product.description
    end

    it "show the product images" do
      product.variant_images.each_with_index do |image|
        expect(response.body).to include image.attachment(:large)
        expect(response.body).to include image.attachment(:small)
      end
    end

    it "renders the show template" do
      expect(response). to render_template :show
    end
  end
end