RSpec.describe ApplicationHelper, type: :helper do
  it "exist the title" do
    expect(full_title("title")). to eq "title - #{ApplicationHelper::BASE_TITLE}"
  end

  it "has no title" do
    expect(full_title("")). to eq ApplicationHelper::BASE_TITLE
  end

  it "has nil" do
    expect(full_title(nil)). to eq ApplicationHelper::BASE_TITLE
  end
end